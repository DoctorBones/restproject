package com.alevel.nix.project.entity.responce;

import com.alevel.nix.project.entity.Role;
import com.alevel.nix.project.entity.User;

import java.util.Set;
import java.util.stream.Collectors;

public class UserResponce {
    private Long id;
    private String username;
    private Set<String> roles;

    public static UserResponce fromUser(User user) {
        return new UserResponce(
                user.getId(),
                user.getUsername(),
                user.getRoles().stream().map(Role::getName).collect(Collectors.toSet())
        );
    }

    public UserResponce(Long id, String username, Set<String> roles) {
        this.id = id;
        this.username = username;
        this.roles = roles;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Set<String> getRoles() {
        return roles;
    }

    public void setRoles(Set<String> roles) {
        this.roles = roles;
    }
}
