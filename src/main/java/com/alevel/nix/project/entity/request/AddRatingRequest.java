package com.alevel.nix.project.entity.request;

public class AddRatingRequest {
    private Long userId;
    private Long postId;
    private String ratingType;

    public AddRatingRequest(Long userId, Long postId, String ratingType) {
        this.userId = userId;
        this.postId = postId;
        this.ratingType = ratingType;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getPostId() {
        return postId;
    }

    public void setPostId(Long postId) {
        this.postId = postId;
    }

    public String getRatingType() {
        return ratingType;
    }

    public void setRatingType(String ratingType) {
        this.ratingType = ratingType;
    }
}
