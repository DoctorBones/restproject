package com.alevel.nix.project.entity.request;

public class SavePostRequest {
    private String header;

    private String description;

    private String picture;

    public SavePostRequest(String header, String description, String picture) {
        this.header = header;
        this.description = description;
        this.picture = picture;
    }

    public SavePostRequest() {
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
