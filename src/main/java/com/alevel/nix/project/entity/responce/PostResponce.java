package com.alevel.nix.project.entity.responce;

import com.alevel.nix.project.entity.Post;
import org.springframework.core.io.FileSystemResource;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class PostResponce {
    private Long id;

    private String header;

    private String description;

    private String picture;

    private String ratingType;

    public static PostResponce fromPost(Post post){
        String rt="No rating yet";
        if(post.getRatingType()!=null){
            rt=post.getRatingType().getName();
        }
        return new PostResponce(post.getId(), post.getHeader(),
                post.getDescription(), post.getPicture(), rt);
    }

    public static List<PostResponce> fromListOfPosts(List<Post> list){
        List<PostResponce> result=new ArrayList<>(list.size());
        for (Post post:list) {
            result.add(fromPost(post));
        }
        return result;
    }



    public PostResponce(Long id, String header, String description, String picture, String ratingType) {
        this.id = id;
        this.header = header;
        this.description = description;
        this.picture = picture;
        this.ratingType=ratingType;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRatingType() {
        return ratingType;
    }

    public void setRatingType(String ratingType) {
        this.ratingType = ratingType;
    }
}
