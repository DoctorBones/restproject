package com.alevel.nix.project.entity.request;

import com.alevel.nix.project.entity.Role;

import java.util.Set;

public class UpdateUserRequest {
    private String password;
    private Set<String> roles;

    public UpdateUserRequest(String password, Set<String> roles) {
        this.password = password;
        this.roles = roles;
    }

    public UpdateUserRequest() {
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<String> getRoles() {
        return roles;
    }

    public void setRoles(Set<String> roles) {
        this.roles = roles;
    }
}
