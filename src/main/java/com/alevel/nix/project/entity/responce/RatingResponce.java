package com.alevel.nix.project.entity.responce;

import com.alevel.nix.project.entity.Rating;

public class RatingResponce {
    private Long id;
    private Long userId;
    private Long postId;
    private String ratingType;

    public static RatingResponce fromRating(Rating rating){
        return new RatingResponce(rating.getId(),rating.getUser().getId(),
                rating.getPost().getId(),rating.getRatingType().getName());
    }

    public RatingResponce(Long id, Long userId, Long postId, String ratingType) {
        this.id = id;
        this.userId = userId;
        this.postId = postId;
        this.ratingType = ratingType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getPostId() {
        return postId;
    }

    public void setPostId(Long postId) {
        this.postId = postId;
    }

    public String getRatingType() {
        return ratingType;
    }

    public void setRatingType(String ratingType) {
        this.ratingType = ratingType;
    }
}
