package com.alevel.nix.project.entity.request;

import java.util.Collections;

public class RegularUserRequest {
    private static final String NAME="USER";

    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserRequest saveRequest(){
        var userRequest=new UserRequest();
        userRequest.setUsername(username);
        userRequest.setPassword(password);
        userRequest.setRoles(Collections.singleton(NAME));
        return userRequest;
    }


}
