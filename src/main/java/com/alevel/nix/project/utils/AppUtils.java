package com.alevel.nix.project.utils;

import com.alevel.nix.project.entity.RatingType;
import com.alevel.nix.project.exception.RatingTypeNotFoundException;
import com.alevel.nix.project.repository.RatingTypeRepository;

import java.util.Optional;

public class AppUtils {
    private final RatingTypeRepository ratingTypeRepository;

    public AppUtils(RatingTypeRepository ratingTypeRepository) {
        this.ratingTypeRepository = ratingTypeRepository;
    }

    public RatingType convertRatingTypeFromString(String rt){

        var ratingType= Optional.of(ratingTypeRepository.findByName(rt));
        if(ratingType.isEmpty()){
            throw new RatingTypeNotFoundException(rt);
        }
        return ratingType.get();

    }
}
