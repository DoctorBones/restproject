package com.alevel.nix.project.service;

import com.alevel.nix.project.entity.Post;
import com.alevel.nix.project.entity.Rating;
import com.alevel.nix.project.entity.request.SavePostRequest;
import com.alevel.nix.project.entity.request.UpdatePostRequest;
import com.alevel.nix.project.exception.PostNotFoundException;
import com.alevel.nix.project.exception.UnavailablePostHeaderException;
import com.alevel.nix.project.repository.PostRepository;
import com.alevel.nix.project.repository.RatingRepository;
import com.alevel.nix.project.repository.RatingTypeRepository;
import com.alevel.nix.project.service.crud.PostCRUD;
import com.alevel.nix.project.utils.AppUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class PostService implements PostCRUD {
    private AppUtils appUtils;
    private final PostRepository postRepository;
    private final RatingRepository ratingRepository;
    private final RatingTypeRepository ratingTypeRepository;
    private static Logger logger= LoggerFactory.getLogger(PostService.class);

    public PostService(PostRepository postRepository, RatingRepository ratingRepository,
                       RatingTypeRepository ratingTypeRepository) {
        this.postRepository = postRepository;
        this.ratingRepository = ratingRepository;
        this.ratingTypeRepository=ratingTypeRepository;
        appUtils=new AppUtils(ratingTypeRepository);
    }

    @Override
    public Post create(SavePostRequest request) {
        String header=request.getHeader();
        if(postRepository.existsByHeader(header)){
            throw new UnavailablePostHeaderException(header);
        }
        Post post=new Post();
        post.setHeader(header);
        post.setDescription(request.getDescription());
        post.setPicture(request.getPicture());
        logger.info("Post with header "+header+", description "+request.getDescription()+
                ", picture"+request.getPicture()+" is created");
        return postRepository.save(post);
    }

    @Override
    public void update(Long id, UpdatePostRequest request) {
        var post= postRepository.findById(id).orElseThrow(()->new PostNotFoundException(id));
        post.setDescription(request.getDescription());
        post.setPicture(request.getPicture());
        logger.info("Post with header "+post.getHeader()+", id"+post.getId()+", " +
                "description "+request.getDescription()+", picture"+request.getPicture() +" is updated");
    }

    @Override
    public Post getById(Long id) {
        return postRepository.findById(id).orElseThrow(()->new PostNotFoundException(id));
    }

    @Override
    public void deleteById(Long id) {
        var post= postRepository.findById(id).orElseThrow(()->new PostNotFoundException(id));
        postRepository.deleteById(id);
        logger.info("Post with header "+post.getHeader()+", description "+
                post.getDescription()+", picture"+post.getPicture()+" is deleted");
    }
    @Override
    public List<Post> getAll(){
        return postRepository.findAll();
    }

    @Override
    public List<Post> getPostsByUser(Long userId) {
        List<Post> result=new ArrayList<>();
        List<Rating> ratings=ratingRepository.findAllByUser_Id(userId);
        for (Rating rating:ratings) {
            result.add(rating.getPost());
        }
        logger.info("Posts found by user id "+userId+" - "+result.size());
        return result;
    }

    @Override
    public List<Post> getPostsByRatingType(String ratingType) {
        return postRepository.findAllByRatingType(appUtils.convertRatingTypeFromString(ratingType));
    }

    @Override
    public List<Post> getPostsByUserAndRatingType(Long userId, String ratingType) {
        List<Post> result=new ArrayList<>();
        List<Rating> ratings=ratingRepository.findAllByUser_Id(userId);
        for (Rating rating:ratings) {
            if (rating.getPost().getRatingType().getName().equals(ratingType)) {
                result.add(rating.getPost());
            }

        }
        logger.info("Posts found by user id "+userId + "and rating type "+ratingType+" - "+result.size());
        return result;
    }
}
