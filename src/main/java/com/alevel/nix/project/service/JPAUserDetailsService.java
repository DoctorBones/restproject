package com.alevel.nix.project.service;

import com.alevel.nix.project.repository.UserRepository;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.stream.Collectors;
@Service
@Transactional
public class JPAUserDetailsService implements UserDetailsService {
    private static final String ROLE = "ROLE_";

    private final UserRepository repository;

    public JPAUserDetailsService(UserRepository repository) {
        this.repository = repository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException{
        var user = repository.findByUsername(username).orElseThrow(()->new UsernameNotFoundException(username));
        return new User(user.getUsername(), user.getPassword(), user.getRoles().stream()
        .map(role->new SimpleGrantedAuthority(ROLE+role.getName())).collect(Collectors.toList()));
    }
}
