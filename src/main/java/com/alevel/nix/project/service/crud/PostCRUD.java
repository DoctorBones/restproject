package com.alevel.nix.project.service.crud;

import com.alevel.nix.project.entity.Post;
import com.alevel.nix.project.entity.request.SavePostRequest;
import com.alevel.nix.project.entity.request.UpdatePostRequest;

import java.util.List;

public interface PostCRUD {
    Post create(SavePostRequest request);
    void update(Long id, UpdatePostRequest request);
    Post getById(Long id);
    void deleteById(Long id);
    List<Post> getAll();
    List<Post> getPostsByUser(Long userId);
    List<Post> getPostsByRatingType(String ratingType);
    List<Post> getPostsByUserAndRatingType(Long userId, String ratingType);
}
