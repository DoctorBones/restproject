package com.alevel.nix.project.service;

import com.alevel.nix.project.entity.Role;
import com.alevel.nix.project.entity.User;
import com.alevel.nix.project.entity.request.UpdateUserRequest;
import com.alevel.nix.project.entity.request.UserRequest;
import com.alevel.nix.project.exception.RoleNotExistsException;
import com.alevel.nix.project.exception.UnavailableUsernameException;
import com.alevel.nix.project.exception.UserNotFoundException;
import com.alevel.nix.project.repository.RoleRepository;
import com.alevel.nix.project.repository.UserRepository;
import com.alevel.nix.project.service.crud.UserCRUD;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

@Transactional
@Service
public class UserService implements UserCRUD {
    private final RoleRepository roleRepository;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    private static Logger logger= LoggerFactory.getLogger(UserService.class);

    public UserService(RoleRepository roleRepository, UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.roleRepository = roleRepository;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public User create(UserRequest request) {
        String username=request.getUsername();
        if(userRepository.existsByUsername(username)){
            throw new UnavailableUsernameException(username);
        }
        User user=new User();
        Set<Role> roles=getRoles(request.getRoles());
        user.setUsername(username);
        user.setPassword(passwordEncoder.encode(request.getPassword()));
        user.setRoles(roles);
        logger.info("User with username "+ username+" is created");
        return userRepository.save(user);
    }

    @Override
    public void update(Long id, UpdateUserRequest request) {
        var user=userRepository.findById(id).orElseThrow(()->new UserNotFoundException(id));

        Set<Role> roles=getRoles(request.getRoles());
        user.setPassword(passwordEncoder.encode(request.getPassword()));
        user.setRoles(roles);
        logger.info("User with username "+user.getUsername()+" is updated");

    }

    @Override
    public User getById(Long id) {
        return userRepository.findById(id).orElseThrow(()->new UserNotFoundException(id));
    }

    @Override
    public void deleteById(Long id) {
        var user=userRepository.findById(id).orElseThrow(()->new UserNotFoundException(id));
        userRepository.deleteById(id);
        logger.info("User with id "+id+" is deleted");
    }

    private Set<Role> getRoles(Set<String> roles){
        Set<Role> result=new HashSet<>(roles.size());
        for(String roleName:roles){
            var role=roleRepository.findByName(roleName).orElseThrow(()->new RoleNotExistsException(roleName));
            result.add(role);
        }
        return result;
    }
}
