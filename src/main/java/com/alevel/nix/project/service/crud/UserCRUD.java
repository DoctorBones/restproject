package com.alevel.nix.project.service.crud;

import com.alevel.nix.project.entity.User;
import com.alevel.nix.project.entity.request.UpdateUserRequest;
import com.alevel.nix.project.entity.request.UserRequest;

import java.util.Optional;

public interface UserCRUD {
    User create(UserRequest request);
    void update(Long id, UpdateUserRequest request);
    User getById(Long id);
    void deleteById(Long id);
}
