package com.alevel.nix.project.service;

import com.alevel.nix.project.entity.Rating;
import com.alevel.nix.project.entity.RatingType;
import com.alevel.nix.project.entity.request.AddRatingRequest;
import com.alevel.nix.project.exception.*;
import com.alevel.nix.project.repository.PostRepository;
import com.alevel.nix.project.repository.RatingRepository;
import com.alevel.nix.project.repository.RatingTypeRepository;
import com.alevel.nix.project.repository.UserRepository;
import com.alevel.nix.project.service.crud.RatingCRUD;
import com.alevel.nix.project.utils.AppUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;
import java.util.Optional;

@Transactional
@Service
public class RatingService implements RatingCRUD {
    private  final RatingTypeRepository ratingTypeRepository;
    private final RatingRepository ratingRepository;
    private final UserRepository userRepository;
    private final PostRepository postRepository;
    private final AppUtils appUtils;
    private static Logger logger= LoggerFactory.getLogger(RatingService.class);
    public RatingService(RatingTypeRepository ratingTypeRepository,
                         RatingRepository ratingRepository,
                         UserRepository userRepository, PostRepository postRepository) {
        this.ratingTypeRepository = ratingTypeRepository;
        this.ratingRepository = ratingRepository;
        this.userRepository = userRepository;
        this.postRepository = postRepository;
        appUtils=new AppUtils(ratingTypeRepository);
    }

    @Override
    public Rating create(AddRatingRequest request) {

        var user=userRepository.findById(request.getUserId()).
                orElseThrow(()->new UserNotFoundException(request.getUserId()));
        var post=postRepository.findById(request.getPostId()).
                orElseThrow(()->new PostNotFoundException(request.getPostId()));
        if(ratingRepository.existsByUser_IdAndPost_Id(request.getUserId(),request.getPostId())){
            throw new RatingAlreadyExistsException(request.getUserId(),request.getPostId());
        }
        var rating=new Rating();
        rating.setUser(user);
        rating.setPost(post);

        var ratingType=appUtils.convertRatingTypeFromString(request.getRatingType());
        rating.setRatingType(ratingType);
        ratingRepository.save(rating);

        updatePost(request.getPostId());
        post.getRatings().add(rating);
        logger.info("Rating with user id "+user.getId()+", post id "+post.getId(), " rating type "+ ratingType + " is created");
        return rating;

    }


    @Override
    public void updateRatingType(AddRatingRequest request) {
        var rating = ratingRepository.findByUser_IdAndPost_Id(request.getUserId(),request.getPostId());
        rating.setRatingType(appUtils.convertRatingTypeFromString(request.getRatingType()));
        ratingRepository.save(rating);
        updatePost(request.getPostId());
        logger.info("Rating with user id "+request.getUserId()+", post id "+request.getPostId()+
                ", rating type "+ request.getRatingType() + " is update");
    }

    @Override
    public void deleteByUserIdAndPostId(Long userId, Long postId) {
        var rating = ratingRepository.findByUser_IdAndPost_Id(userId,postId);
        if(rating==null){
            throw new RatingNotFoundException(userId, postId);
        }
        ratingRepository.delete(rating);
        updatePost(postId);
        logger.info("Rating with user id "+userId+", post id "+postId+
                " is deleted");
    }

    @Override
    public void deleteById(Long id) {
        var rating=ratingRepository.findById(id).orElseThrow(()->new RatingNotFoundException(id));
        ratingRepository.delete(rating);
        updatePost(rating.getPost().getId());
        logger.info("Rating with id " + id + " is deleted");
    }

    @Override
    public Rating findByUserIdAndPostId(Long userId, Long postId) {
        Rating rating=ratingRepository.findByUser_IdAndPost_Id(userId,postId);
        if(rating==null){
            throw new RatingNotFoundException(userId,postId);
        }
        logger.info("Rating with userId "+ userId+" and post id "+postId+" is deleted");
        return rating;
    }



    private void updatePost(Long postId){
        var ratings=ratingRepository.findAllByPost_Id(postId);
        float sum=0f;
        for (Rating rating:ratings) {
            sum+=ratingTypeRepository.findByName(rating.getRatingType().getName()).getId();
        }
        sum/=ratings.size();
        var post=postRepository.findById(postId);
        var ratingTypeId=Long.valueOf(Math.round(sum));
        if(ratingTypeId==0){
            post.get().setRatingType(ratingTypeRepository.findByName("UNSIGNED"));
        }
        else {
            post.get().setRatingType(ratingTypeRepository.findById(ratingTypeId).
                    orElseThrow(() -> new RatingTypeNotFoundException(Long.valueOf(Math.round(ratingTypeId)))));
        }
        logger.info("Post's rating type is updated - "+post.get().getRatingType().getName());

    }
}
