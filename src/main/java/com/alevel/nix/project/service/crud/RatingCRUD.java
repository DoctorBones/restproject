package com.alevel.nix.project.service.crud;

import com.alevel.nix.project.entity.Rating;
import com.alevel.nix.project.entity.request.AddRatingRequest;

import java.util.List;

public interface RatingCRUD {
    Rating create(AddRatingRequest request);
    void updateRatingType(AddRatingRequest request);
    void deleteByUserIdAndPostId(Long userId, Long postId);
    void deleteById(Long id);
    Rating findByUserIdAndPostId(Long userId, Long postId);
}
