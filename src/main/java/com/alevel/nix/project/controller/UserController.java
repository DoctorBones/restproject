package com.alevel.nix.project.controller;

import com.alevel.nix.project.entity.request.RegularUserRequest;
import com.alevel.nix.project.entity.request.UpdateUserRequest;
import com.alevel.nix.project.entity.responce.UserResponce;
import com.alevel.nix.project.exception.UnavailableUsernameException;
import com.alevel.nix.project.service.crud.UserCRUD;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

@RestController
@RequestMapping("/users")
public class UserController {
    private final UserCRUD userCRUD;

    public UserController(UserCRUD userCRUD) {
        this.userCRUD = userCRUD;
    }

    @PostMapping
    public ResponseEntity<UserResponce> create(@RequestBody RegularUserRequest regularUserRequest){
        var saveUser=regularUserRequest.saveRequest();
        var user=userCRUD.create(saveUser);
        return ResponseEntity.created(URI.create("/users/"+user.getId())).body(UserResponce.fromUser(user));
    }

    @GetMapping("/{id}")
    public UserResponce getById(@PathVariable Long id){
        var user = userCRUD.getById(id);
        return UserResponce.fromUser(user);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@PathVariable Long id, @RequestBody UpdateUserRequest request){
        userCRUD.update(id, request);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteById(@PathVariable Long id){
        userCRUD.deleteById(id);
    }
}
