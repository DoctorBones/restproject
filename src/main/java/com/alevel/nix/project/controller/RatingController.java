package com.alevel.nix.project.controller;

import com.alevel.nix.project.entity.Rating;
import com.alevel.nix.project.entity.request.AddRatingRequest;
import com.alevel.nix.project.entity.responce.RatingResponce;
import com.alevel.nix.project.service.crud.RatingCRUD;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

@RestController
@RequestMapping("/ratings")
public class RatingController {
    private final RatingCRUD ratingCRUD;

    public RatingController(RatingCRUD ratingCRUD) {
        this.ratingCRUD = ratingCRUD;
    }


    @PostMapping
    public ResponseEntity<RatingResponce> create(@RequestBody AddRatingRequest ratingRequest){
        var rating=ratingCRUD.create(ratingRequest);
        return ResponseEntity.created(URI.create("/ratings/"+rating.getId())).body(RatingResponce.fromRating(rating));
    }

    @GetMapping("/userId={userId}&postId={postId}")
    public RatingResponce get(@PathVariable Long userId, @PathVariable Long postId){
        var rating = ratingCRUD.findByUserIdAndPostId(userId, postId);
        return RatingResponce.fromRating(rating);
    }

    @PutMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@RequestBody AddRatingRequest request){
        ratingCRUD.updateRatingType(request);
    }

    @DeleteMapping("/deleteById/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteById(@PathVariable Long id){
        ratingCRUD.deleteById(id);
    }

    @DeleteMapping("/userId={userId}&postId={postId}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteByUserIdAndPostId(@PathVariable Long userId, @PathVariable Long postId){
        ratingCRUD.deleteByUserIdAndPostId(userId, postId);
    }
}
