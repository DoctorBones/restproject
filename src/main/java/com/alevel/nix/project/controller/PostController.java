package com.alevel.nix.project.controller;

import com.alevel.nix.project.entity.Post;
import com.alevel.nix.project.entity.request.SavePostRequest;
import com.alevel.nix.project.entity.request.UpdatePostRequest;
import com.alevel.nix.project.entity.responce.PostResponce;
import com.alevel.nix.project.service.crud.PostCRUD;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/posts")
public class PostController {
    private final PostCRUD postCRUD;

    public PostController(PostCRUD postCRUD) {
        this.postCRUD = postCRUD;
    }

    @GetMapping("/byRating")
    public List<PostResponce> getByRatingType(@RequestParam(name="ratingType") String ratingType){
        return PostResponce.fromListOfPosts(postCRUD.getPostsByRatingType(ratingType));
    }

    @GetMapping("/byUser/{userId}")
    public List<PostResponce> getByUserId(@PathVariable Long userId){
        return PostResponce.fromListOfPosts(postCRUD.getPostsByUser(userId));
    }

    @GetMapping("/byUserAndRating")
    public List<PostResponce> getByUserIdAndRatingType(@RequestParam(name="userId") Long userId,
                                                       @RequestParam(name="ratingType") String ratingType){
        return PostResponce.fromListOfPosts(postCRUD.getPostsByUserAndRatingType(userId,ratingType));
    }

    @GetMapping
    public List<PostResponce> getAll(){
        return PostResponce.fromListOfPosts(postCRUD.getAll());
    }

    @PostMapping
    public ResponseEntity<PostResponce> create(@RequestBody SavePostRequest postRequest){
        var post=postCRUD.create(postRequest);
        return ResponseEntity.created(URI.create("/posts/postId/"+post.getId())).body(PostResponce.fromPost(post));
    }

    @GetMapping("/postId/{id}")
    public PostResponce getById(@PathVariable Long id){
        var post = postCRUD.getById(id);
        return PostResponce.fromPost(post);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@PathVariable Long id, @RequestBody UpdatePostRequest request){
        postCRUD.update(id, request);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteById(@PathVariable Long id){
        postCRUD.deleteById(id);
    }
}
