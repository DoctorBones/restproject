package com.alevel.nix.project.repository;

import com.alevel.nix.project.entity.Post;
import com.alevel.nix.project.entity.RatingType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PostRepository extends JpaRepository<Post, Long> {
    boolean existsByHeader(String header);
    List<Post> findAllByRatingType(RatingType ratingType);
}
