package com.alevel.nix.project.repository;

import com.alevel.nix.project.entity.Rating;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RatingRepository extends JpaRepository<Rating, Long> {
    Rating findByUser_IdAndPost_Id(Long userId, Long postId);
    boolean existsByUser_IdAndPost_Id(Long userId, Long postId);
    List<Rating> findAllByPost_Id(Long postId);
    List<Rating> findAllByUser_Id(Long userId);

}
