package com.alevel.nix.project.repository;

import com.alevel.nix.project.entity.RatingType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RatingTypeRepository extends JpaRepository<RatingType, Long> {
    RatingType findByName(String name);
    boolean existsByName(String name);
}
