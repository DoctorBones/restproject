package com.alevel.nix.project.exception;

import org.springframework.http.HttpStatus;

public class RatingNotFoundException extends AppStatusException {
    public RatingNotFoundException(Long userId, Long postId){
        super(HttpStatus.NOT_FOUND, "Rating with user id "+ userId +" and with post id "+ postId+" was not found");
    }

    public RatingNotFoundException(Long id){
        super(HttpStatus.NOT_FOUND, "Rating with id " + id+" not found");

    }
}
