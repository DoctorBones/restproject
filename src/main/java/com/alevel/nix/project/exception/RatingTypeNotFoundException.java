package com.alevel.nix.project.exception;

import org.springframework.http.HttpStatus;

public class RatingTypeNotFoundException extends AppStatusException {
    public RatingTypeNotFoundException(String name){
        super(HttpStatus.NOT_FOUND, "Rating type " + name+" not found");
    }

    public RatingTypeNotFoundException(Long id){
        super(HttpStatus.NOT_FOUND, "Rating type with id " + id+" not found");
    }
}
