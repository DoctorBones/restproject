package com.alevel.nix.project.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class UnavailableUsernameException extends AppStatusException {
    public UnavailableUsernameException(String username){
        super(HttpStatus.BAD_REQUEST, "Username "+username + " has already been taken");
    }

}
