package com.alevel.nix.project.exception;

import org.springframework.http.HttpStatus;

public class RatingAlreadyExistsException extends AppStatusException {
    public RatingAlreadyExistsException(Long userId, Long postId){
        super(HttpStatus.BAD_REQUEST, "Rating with user id "+ userId +" and with post id "+ postId+" was not found");
    }
}
