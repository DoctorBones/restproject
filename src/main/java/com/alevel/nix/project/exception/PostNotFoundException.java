package com.alevel.nix.project.exception;

import org.springframework.http.HttpStatus;

public class PostNotFoundException extends AppStatusException {
    public PostNotFoundException(String name){
        super(HttpStatus.NOT_FOUND, "Post with header " + name+" not found");
    }

    public PostNotFoundException(Long id){
        super(HttpStatus.NOT_FOUND, "Post with id " + id+" not found");
    }

}
