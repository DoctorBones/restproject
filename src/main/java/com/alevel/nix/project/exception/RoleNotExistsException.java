package com.alevel.nix.project.exception;

import org.springframework.http.HttpStatus;

public class RoleNotExistsException extends AppStatusException {
    public RoleNotExistsException(String role){
        super(HttpStatus.NOT_FOUND, role + "not found");
    }
}
