package com.alevel.nix.project.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class AppStatusException extends ResponseStatusException {
    public AppStatusException(HttpStatus status) {
        super(status);
    }

    public AppStatusException(HttpStatus status, String reason) {
        super(status, reason);
    }

    public AppStatusException(HttpStatus status, String reason, Throwable cause) {
        super(status, reason, cause);
    }
}
