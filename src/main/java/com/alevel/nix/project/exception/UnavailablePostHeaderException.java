package com.alevel.nix.project.exception;

import org.springframework.http.HttpStatus;

public class UnavailablePostHeaderException extends AppStatusException {
    public UnavailablePostHeaderException(String header){
        super(HttpStatus.BAD_REQUEST, "Post "+header + " has already been taken");
    }
}
