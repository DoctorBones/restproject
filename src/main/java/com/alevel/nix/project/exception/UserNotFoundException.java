package com.alevel.nix.project.exception;

import org.springframework.http.HttpStatus;

public class UserNotFoundException extends AppStatusException {
    public UserNotFoundException(String name){
        super(HttpStatus.NOT_FOUND, "User with name " + name+" not found");
    }

    public UserNotFoundException(Long id){
        super(HttpStatus.NOT_FOUND, "User with id " + id+" not found");
    }
}
