insert into roles (name) values ('USER');
insert into roles (name) values ('ADMIN');

insert into rating_type (name)
values
        ('VERY_NICE'),
        ('NICE'),
        ('NORMAL'),
        ('SCARY'),
        ('VERY_SCARY'),
        ('UNSIGNED');

