package com.alevel.nix.project;


import com.alevel.nix.project.entity.Rating;
import com.alevel.nix.project.entity.request.AddRatingRequest;
import com.alevel.nix.project.entity.request.RegularUserRequest;
import com.alevel.nix.project.entity.request.SavePostRequest;
import com.alevel.nix.project.entity.responce.PostResponce;
import com.alevel.nix.project.entity.responce.RatingResponce;

import com.alevel.nix.project.entity.responce.UserResponce;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;

import java.net.URI;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ProjectRatingsTest {
    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate rest;

    private TestUtils testUtils;

    @BeforeEach
    void init(){
        testUtils=new TestUtils(rest, port);
    }

    @Test
    void contextLoads(){
        assertNotNull(rest);
        assertNotEquals(0,port);
    }







    @Test
    void testCreateRating(){
        String username="usernameCreate";
        String password="password";
        UserResponce user= testUtils.createUser(username, password).getBody();
        String header="TestCreate";
        String description="Test description";
        String picture = "Test picture";
        PostResponce post= testUtils.createPost(header,description,picture).getBody();
        String ratingType="NICE";
        ResponseEntity<RatingResponce> responseEntity= testUtils.createRating(user.getId(), post.getId(), ratingType);
        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        assertEquals(MediaType.APPLICATION_JSON, responseEntity.getHeaders().getContentType());
        RatingResponce ratingResponce=responseEntity.getBody();
        assertNotNull(ratingResponce);
        assertEquals(user.getId(), ratingResponce.getUserId());
        assertEquals(post.getId(), ratingResponce.getPostId());
        assertEquals(ratingType, ratingResponce.getRatingType());
    }

    @Test
    void getRating(){
        String username="usernameGet";
        String password="password";
        UserResponce user= testUtils.createUser(username, password).getBody();
        String header="TestGet";
        String description="Test description";
        String picture = "Test picture";
        PostResponce post= testUtils.createPost(header,description,picture).getBody();
        String ratingType="NICE";
        RatingResponce rating = testUtils.createRating(user.getId(), post.getId(), ratingType).getBody();
        Long id=rating.getId();

        String messageUrl = "http://localhost:" + port + "/ratings/userId="+user.getId()+"&postId="+post.getId();

        ResponseEntity<RatingResponce> messageResponseEntity = rest
                .getForEntity(messageUrl, RatingResponce.class);
        assertEquals(HttpStatus.OK, messageResponseEntity.getStatusCode());
        assertEquals(MediaType.APPLICATION_JSON, messageResponseEntity.getHeaders().getContentType());

        RatingResponce responseBody = messageResponseEntity.getBody();
        assertNotNull(responseBody);
        assertEquals(user.getId(), responseBody.getUserId());
        assertEquals(post.getId(), responseBody.getPostId());
        assertEquals(ratingType, responseBody.getRatingType());
    }

    @Test
    void testGetNonExistingRating() {
        Random random=new Random();
        String messageUrl = "http://localhost:" + port + "/ratings/userId=" + random.nextLong()+"&postId="+random.nextLong();
        ResponseEntity<PostResponce> postResponseEntity = rest.getForEntity(messageUrl, PostResponce.class);
        assertEquals(HttpStatus.NOT_FOUND, postResponseEntity.getStatusCode());
    }

    @Test
    void updateRatingWithRatingType(){
        String username="usernameUpdate";
        String password="password";
        UserResponce user= testUtils.createUser(username, password).getBody();
        String header="TestUpdate";
        String description="Test description";
        String picture = "Test picture";
        PostResponce post= testUtils.createPost(header,description,picture).getBody();
        String ratingType="NICE";

        RatingResponce rating = testUtils.createRating(user.getId(), post.getId(), ratingType).getBody();

        String messageUrl = "http://localhost:" + port + "/ratings";

        String messageUrlGet = "http://localhost:" + port + "/ratings/userId="+user.getId()+"&postId="+post.getId();
        String uptdate="VERY_NICE";

        rest.put(messageUrl, new AddRatingRequest(user.getId(),post.getId(),uptdate));

        ResponseEntity<RatingResponce> messageResponseEntity = rest.getForEntity(messageUrlGet, RatingResponce.class);
        assertEquals(HttpStatus.OK, messageResponseEntity.getStatusCode());
        assertEquals(MediaType.APPLICATION_JSON, messageResponseEntity.getHeaders().getContentType());

        RatingResponce responseBody = messageResponseEntity.getBody();
        assertNotNull(responseBody);

        assertEquals(uptdate, responseBody.getRatingType());
        assertEquals(user.getId(), responseBody.getUserId());
        assertEquals(post.getId(), responseBody.getPostId());

    }

    @Test
    void deleteById(){
        String username="usernameDelete";
        String password="password";
        UserResponce user= testUtils.createUser(username, password).getBody();
        String header="TestDelete";
        String description="Test description";
        String picture = "Test picture";
        PostResponce post= testUtils.createPost(header,description,picture).getBody();
        String ratingType="NICE";

        RatingResponce rating = testUtils.createRating(user.getId(), post.getId(), ratingType).getBody();

        Long id=rating.getId();
        String messageUrl = "http://localhost:" + port + "/ratings/deleteById/" + id;
        String messageUrlGet = "http://localhost:" + port + "/ratings/userId="+user.getId()+"&postId="+post.getId();
        URI messageUri = URI.create(messageUrl);

        ResponseEntity<RatingResponce> messageResponseEntity = rest
                .exchange(RequestEntity.delete(messageUri).build(), RatingResponce.class);

        assertEquals(HttpStatus.OK, messageResponseEntity.getStatusCode());

        assertEquals(HttpStatus.NOT_FOUND, rest.getForEntity(messageUrlGet,RatingResponce.class).getStatusCode());
        assertEquals(HttpStatus.NOT_FOUND, rest
                .exchange(RequestEntity.delete(messageUri).build(), RatingResponce.class)
                .getStatusCode());
    }

    @Test
    void deleteByUserIdAndPostId(){
        String username="usernameDeleteByUser";
        String password="password";
        UserResponce user= testUtils.createUser(username, password).getBody();
        String header="TestDeleteByUser";
        String description="Test description";
        String picture = "Test picture";
        PostResponce post= testUtils.createPost(header,description,picture).getBody();
        String ratingType="NICE";

        RatingResponce rating = testUtils.createRating(user.getId(), post.getId(), ratingType).getBody();

        String messageUrl = "http://localhost:" + port + "/ratings/userId="+user.getId()+"&postId="+post.getId();
        String messageUrlGet = "http://localhost:" + port + "/ratings/userId="+user.getId()+"&postId="+post.getId();
        URI messageUri = URI.create(messageUrl);

        ResponseEntity<RatingResponce> messageResponseEntity = rest
                .exchange(RequestEntity.delete(messageUri).build(), RatingResponce.class);

        assertEquals(HttpStatus.OK, messageResponseEntity.getStatusCode());

        assertEquals(HttpStatus.NOT_FOUND, rest.getForEntity(messageUrlGet,RatingResponce.class).getStatusCode());
        assertEquals(HttpStatus.NOT_FOUND, rest
                .exchange(RequestEntity.delete(messageUri).build(), RatingResponce.class)
                .getStatusCode());
    }

}
