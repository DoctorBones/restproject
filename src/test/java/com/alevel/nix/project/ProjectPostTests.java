package com.alevel.nix.project;

import com.alevel.nix.project.entity.Post;
import com.alevel.nix.project.entity.User;
import com.alevel.nix.project.entity.request.SavePostRequest;
import com.alevel.nix.project.entity.request.UpdatePostRequest;
import com.alevel.nix.project.entity.responce.PostResponce;
import com.alevel.nix.project.entity.responce.RatingResponce;
import com.alevel.nix.project.entity.responce.UserResponce;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;

import java.net.URI;
import java.util.List;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ProjectPostTests {

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate rest;

	private TestUtils testUtils;

	@BeforeEach
	void init(){
		testUtils=new TestUtils(rest, port);
	}

	@Test
	void contextLoads(){
		assertNotNull(rest);
		assertNotEquals(0,port);
	}



	@Test
	void testCreatePost(){
		String header="Test1";
		String description="Test description";
		String picture = "Test picture";
		ResponseEntity<PostResponce> responseEntity= testUtils.createPost(header,description,picture);
		assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
		assertEquals(MediaType.APPLICATION_JSON, responseEntity.getHeaders().getContentType());
		PostResponce post=responseEntity.getBody();
		assertNotNull(post);
		assertEquals(header,post.getHeader());
		assertEquals(description, post.getDescription());
		assertEquals(picture, post.getPicture());

	}

	@Test
	void getPostId(){
		String header = "Get";
		String description = "Description";
		String picture="Picture";

		PostResponce post = testUtils.createPost(header,description,picture).getBody();
		Long id=post.getId();

		String messageUrl = "http://localhost:" + port + "/posts/postId/" + id;

		ResponseEntity<PostResponce> messageResponseEntity = rest
				.getForEntity(messageUrl, PostResponce.class);
		assertEquals(HttpStatus.OK, messageResponseEntity.getStatusCode());
		assertEquals(MediaType.APPLICATION_JSON, messageResponseEntity.getHeaders().getContentType());

		PostResponce responseBody = messageResponseEntity.getBody();
		assertNotNull(responseBody);
		assertEquals(header, responseBody.getHeader());
		assertEquals(description, responseBody.getDescription());
		assertEquals(id, responseBody.getId());
	}

	@Test
	void testGetNonExistingToDo() {
		Random random=new Random();
		String messageUrl = "http://localhost:" + port + "/posts/postId/" + random.nextLong();
		ResponseEntity<PostResponce> postResponseEntity = rest.getForEntity(messageUrl, PostResponce.class);
		assertEquals(HttpStatus.NOT_FOUND, postResponseEntity.getStatusCode());
	}


	@Test
	void updatePost(){
		String header = "Update";
		String description = "Description";
		String picture="Picture";

		PostResponce post = testUtils.createPost(header, description, picture).getBody();

		Long id = post.getId();

		String messageUrl = "http://localhost:" + port + "/posts/" + id;

		String messageUrlGet="http://localhost:" + port + "/posts/postId/" + id;
		String updateDescription = "updated description";
		String updatePicture = "updated picture";

		rest.put(messageUrl, new UpdatePostRequest(updateDescription, updatePicture));

		ResponseEntity<PostResponce> messageResponseEntity = rest.getForEntity(messageUrlGet, PostResponce.class);
		assertEquals(HttpStatus.OK, messageResponseEntity.getStatusCode());
		assertEquals(MediaType.APPLICATION_JSON, messageResponseEntity.getHeaders().getContentType());

		PostResponce responseBody = messageResponseEntity.getBody();
		assertNotNull(responseBody);

		assertEquals(updateDescription, responseBody.getDescription());
		assertEquals(updatePicture, responseBody.getPicture());
		assertEquals(id, responseBody.getId());
	}

	@Test
	void deletePost(){
		String header = "GetDelete";
		String description = "Description";
		String picture="Picture";
		PostResponce post = testUtils.createPost(header, description, picture).getBody();

		Long id = post.getId();

		String messageUrl = "http://localhost:" + port + "/posts/" + id;
		URI messageUri = URI.create(messageUrl);

		ResponseEntity<PostResponce> messageResponseEntity = rest
				.exchange(RequestEntity.delete(messageUri).build(), PostResponce.class);

		assertEquals(HttpStatus.OK, messageResponseEntity.getStatusCode());
		String messageUrlGet="http://localhost:" + port + "/posts/postId/" + id;

		assertEquals(HttpStatus.NOT_FOUND, rest.getForEntity(messageUrlGet,PostResponce.class).getStatusCode());
		assertEquals(HttpStatus.NOT_FOUND, rest
				.exchange(RequestEntity.delete(messageUri).build(), PostResponce.class)
				.getStatusCode());
	}



	@Test
	public void getByUsers(){
		String username="usernameGetByUsers";
		String password="password";
		UserResponce user= testUtils.createUser(username, password).getBody();
		String header="TestGetByUsers";
		String description="Test description";
		String picture = "Test picture";
		PostResponce post= testUtils.createPost(header,description,picture).getBody();
		ResponseEntity<PostResponce> postWithoutRating=testUtils.createPost("Another post","Without","Rating");
		String ratingType="NICE";
		RatingResponce rating = testUtils.createRating(user.getId(), post.getId(), ratingType).getBody();
		String messageUrl = "http://localhost:" + port + "/posts/byUser/"+user.getId();
		ResponseEntity<PostResponce[]> messageResponseEntity = rest
				.getForEntity(messageUrl, PostResponce[].class);
		assertEquals(HttpStatus.OK, messageResponseEntity.getStatusCode());
		assertEquals(MediaType.APPLICATION_JSON, messageResponseEntity.getHeaders().getContentType());
		PostResponce[] list=messageResponseEntity.getBody();
		assertNotNull(list);
		assertEquals(1, list.length);
		PostResponce responce=list[0];
		assertEquals(header, responce.getHeader());
		assertEquals(description, responce.getDescription());
		assertEquals(picture, responce.getPicture());
		assertEquals(ratingType, responce.getRatingType());

	}

	@Test
	public void getByRating() {
		String username="usernameGetByRating";
		String password="password";
		UserResponce user= testUtils.createUser(username, password).getBody();
		String header="TestGetByRating";
		String description="Test description";
		String picture = "Test picture";
		PostResponce post= testUtils.createPost(header,description,picture).getBody();
		ResponseEntity<PostResponce> postWithoutRating=testUtils.createPost("Another post1","Without","Rating");
		String ratingType="VERY_NICE";
		RatingResponce rating = testUtils.createRating(user.getId(), post.getId(), ratingType).getBody();
		String messageUrl = "http://localhost:" + port + "/posts/byRating?ratingType="+ratingType;
		ResponseEntity<PostResponce[]> messageResponseEntity = rest
				.getForEntity(messageUrl, PostResponce[].class);
		assertEquals(HttpStatus.OK, messageResponseEntity.getStatusCode());
		assertEquals(MediaType.APPLICATION_JSON, messageResponseEntity.getHeaders().getContentType());
		PostResponce[] list=messageResponseEntity.getBody();
		assertNotNull(list);
		assertEquals(1, list.length);
		PostResponce responce=list[0];
		assertEquals(header, responce.getHeader());
		assertEquals(description, responce.getDescription());
		assertEquals(picture, responce.getPicture());
		assertEquals(ratingType, responce.getRatingType());
	}

	@Test
	public void getByRatingAndUser() {
		String username="usernameGetByRatingAndUser";
		String password="password";
		UserResponce user= testUtils.createUser(username, password).getBody();
		String header="TestGetByRatingAndUser";
		String description="Test description";
		String picture = "Test picture";
		PostResponce post= testUtils.createPost(header,description,picture).getBody();
		ResponseEntity<PostResponce> postWithoutRating=testUtils.createPost("Another post2","Without","Rating");
		String ratingType="NICE";
		RatingResponce rating = testUtils.createRating(user.getId(), post.getId(), ratingType).getBody();
		String messageUrl = "http://localhost:" + port + "/posts/byUserAndRating?userId="+user.getId()+"&ratingType="+ratingType;
		ResponseEntity<PostResponce[]> messageResponseEntity = rest
				.getForEntity(messageUrl, PostResponce[].class);
		assertEquals(HttpStatus.OK, messageResponseEntity.getStatusCode());
		assertEquals(MediaType.APPLICATION_JSON, messageResponseEntity.getHeaders().getContentType());
		PostResponce[] list=messageResponseEntity.getBody();
		assertNotNull(list);
		assertEquals(1, list.length);
		PostResponce responce=list[0];
		assertEquals(header, responce.getHeader());
		assertEquals(description, responce.getDescription());
		assertEquals(picture, responce.getPicture());
		assertEquals(ratingType, responce.getRatingType());
	}


}
