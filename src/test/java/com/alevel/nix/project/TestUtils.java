package com.alevel.nix.project;

import com.alevel.nix.project.entity.request.AddRatingRequest;
import com.alevel.nix.project.entity.request.RegularUserRequest;
import com.alevel.nix.project.entity.request.SavePostRequest;
import com.alevel.nix.project.entity.responce.PostResponce;
import com.alevel.nix.project.entity.responce.RatingResponce;
import com.alevel.nix.project.entity.responce.UserResponce;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;

public class TestUtils {
    private TestRestTemplate rest;
    private int port;

    public TestUtils(TestRestTemplate rest, int port) {
        this.rest = rest;
        this.port = port;
    }

    public ResponseEntity<UserResponce> createUser(String username, String password){
        String url="http://localhost:"+port+"/users";
        RegularUserRequest request=new RegularUserRequest();
        request.setUsername(username);
        request.setPassword(password);
        return rest.postForEntity(url,request, UserResponce.class);
    }

    public ResponseEntity<PostResponce> createPost(String header, String description, String picture){
        String url="http://localhost:"+port+"/posts";
        SavePostRequest request=new SavePostRequest();
        request.setHeader(header);
        request.setDescription(description);
        request.setPicture(picture);
        return rest.postForEntity(url,request, PostResponce.class);
    }

    public ResponseEntity<RatingResponce> createRating(Long userId, Long ratingId, String ratingType) {
        String url = "http://localhost:" + port + "/ratings";
        AddRatingRequest request = new AddRatingRequest(userId, ratingId, ratingType);
        return rest.postForEntity(url,request, RatingResponce.class);
    }
}
