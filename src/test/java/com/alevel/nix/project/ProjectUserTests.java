package com.alevel.nix.project;

import com.alevel.nix.project.entity.request.RegularUserRequest;
import com.alevel.nix.project.entity.request.UpdateUserRequest;
import com.alevel.nix.project.entity.request.UserRequest;

import com.alevel.nix.project.entity.responce.PostResponce;
import com.alevel.nix.project.entity.responce.UserResponce;
import com.alevel.nix.project.utils.AppUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;

import java.net.URI;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ProjectUserTests {
    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate rest;

    private TestUtils testUtils;

    @Test
    void contextLoads(){
        assertNotNull(rest);
        assertNotEquals(0,port);
    }


    @BeforeEach
    void init(){
        testUtils=new TestUtils(rest,port);
    }

    @Test
    void testCreateUser(){
        String username="username";
        String password="password";

        ResponseEntity<UserResponce> responseEntity= testUtils.createUser(username, password);
        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        assertEquals(MediaType.APPLICATION_JSON, responseEntity.getHeaders().getContentType());
        UserResponce user=responseEntity.getBody();
        assertNotNull(user);
        assertEquals(username, user.getUsername());
    }


    @Test
    void getPostId(){
        String username="usernameGet";
        String password="password";

        UserResponce user = testUtils.createUser(username, password).getBody();
        Long id=user.getId();

        String messageUrl = "http://localhost:" + port + "/users/" + id;

        ResponseEntity<UserResponce> messageResponseEntity = rest
                .getForEntity(messageUrl, UserResponce.class);
        assertEquals(HttpStatus.OK, messageResponseEntity.getStatusCode());
        assertEquals(MediaType.APPLICATION_JSON, messageResponseEntity.getHeaders().getContentType());

        UserResponce responseBody = messageResponseEntity.getBody();
        assertNotNull(responseBody);
        assertEquals(username, responseBody.getUsername());
        assertEquals(id, responseBody.getId());
    }

    @Test
    void testGetNonExistingUser() {
        Random random=new Random();
        String messageUrl = "http://localhost:" + port + "/users/" + random.nextLong();
        ResponseEntity<PostResponce> postResponseEntity = rest.getForEntity(messageUrl, PostResponce.class);
        assertEquals(HttpStatus.NOT_FOUND, postResponseEntity.getStatusCode());
    }

    @Test
    void updateUser(){
        String username="usernameUpdate";
        String password="password";

        UserResponce user = testUtils.createUser(username, password).getBody();
        assertNotNull(user);

        Long id = user.getId();

        String messageUrl = "http://localhost:" + port + "/users/" + id;

        Set<String> roles=new HashSet<>();
        roles.add("USER");
        roles.add("ADMIN");
        rest.put(messageUrl, new UpdateUserRequest(password, roles));

        ResponseEntity<UserResponce> messageResponseEntity = rest.getForEntity(messageUrl, UserResponce.class);
        assertEquals(HttpStatus.OK, messageResponseEntity.getStatusCode());
        assertEquals(MediaType.APPLICATION_JSON, messageResponseEntity.getHeaders().getContentType());

        UserResponce responseBody = messageResponseEntity.getBody();
        assertNotNull(responseBody);
        assertEquals(roles, responseBody.getRoles());
    }



    @Test
    void deleteUser(){
        String username="usernameDelete";
        String password="password";
        UserResponce user = testUtils.createUser(username, password).getBody();

        Long id = user.getId();

        String messageUrl = "http://localhost:" + port + "/users/" + id;
        URI messageUri = URI.create(messageUrl);

        ResponseEntity<PostResponce> messageResponseEntity = rest
                .exchange(RequestEntity.delete(messageUri).build(), PostResponce.class);

        assertEquals(HttpStatus.OK, messageResponseEntity.getStatusCode());


        assertEquals(HttpStatus.NOT_FOUND, rest.getForEntity(messageUrl,PostResponce.class).getStatusCode());
        assertEquals(HttpStatus.NOT_FOUND, rest
                .exchange(RequestEntity.delete(messageUri).build(), PostResponce.class)
                .getStatusCode());
    }

}
